/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vetexprise.javafx.example.http;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import org.vetexprise.javafx.example.table.Student;


/**
 * Клиент для подключения к HTTP серверу
 *
 * @author vaganovdv
 */
public class JavaFXHttpClient {

    private final  HttpClient client;       // Клиент Http (REST)
    private final  ObjectMapper mapper ;    // Класс для работы с JSON форматом
    
    // Указания типа данных, приходящих в строке ответа 
    private final  TypeReference studentListType = new TypeReference<List<Student>>(){};
    
    
   
    public JavaFXHttpClient() {        
        System.out.println("Инициализация экземпляра Http клиента ...");
        this.client = HttpClient.newHttpClient();
        System.out.println("Создан экземпляр Http клиента");
        
        System.out.println("Инициализация ObjectMapper ...");
        mapper = new ObjectMapper();        
        System.out.println("Инициализация ObjectMapper завершена");
        
    }
    
    /**
     * Получение полного списка студентов
     * 
     * @return 
     */
    public List<Student> getAllStudents() {

        // Список студентов
        List<Student> list = new ArrayList<>();
        
        
        // Фабрика  запросов 
        HttpRequest request = HttpRequest.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .uri(URI.create("http://localhost:7777/student"))
                .build();

        try {
            
            // Клиентский запрос к серверц
            HttpResponse<String> response = client.send(request, BodyHandlers.ofString());

            // Ответ от сервера
            String responseBody = response.body();
            int responseStatusCode = response.statusCode();

            System.out.println("Получен ответ от сервера: " + responseBody);
            System.out.println("Код статуса httpGetRequest status code: " + responseStatusCode);

            
            // Преобразование строки в экземпляры класса Student
            List<Student> respList = (List<Student>) mapper.readValue(responseBody, studentListType);
            if (respList != null && !respList.isEmpty()) {
                list.addAll(respList);
            }

            System.out.println("Прочитан список [" + list.size() + "] студентов ");

        } catch (IOException ex) {
            System.out.println("Ошибка выполнения GET запроса: " + ex.toString());

        } catch (InterruptedException ex) {
            System.out.println("Ошибка выполнения GET запроса: " + ex.toString());
        }
        return list;

    }

}
